<?php

class Subject implements \SplSubject
{
    private $observers;

    public function __construct()
    {
        $this->observers = new \SplObjectStorage();
    }

    public function attach(\SplObserver $observer)
    {
        echo "Attached an observer.\n";
        $this->observers->attach($observer);
    }

    public function detach(\SplObserver $observer)
    {
        $this->observers->detach($observer);
        echo "Detached an observer.\n";
    }

    public function notify()
    {
        echo "Notifying update...\n";
        foreach ($this->observers as $observer) {
            $observer->update($this);
        }
    }
}
