<?php
require_once "Subject.php";
require_once "Observer.php";

$subject = new Subject();
$observer = new Observer();
$subject->attach($observer);
$subject->notify();
$subject->detach($observer);
$subject->notify();
