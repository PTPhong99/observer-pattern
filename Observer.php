<?php

class Observer implements \SplObserver
{
    public function update(\SplSubject $subject)
    {
        echo "Observer: Reacted to the event.\n";
    }
}
